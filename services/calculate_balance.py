import json

import os

def calculate_balance(filename):
    if (os.stat(filename).st_size == 0):
        return []
    current_file = open(filename)
    data = json.load(current_file)
    current_file.close()

    soma = 0
    # print({ k: v for k, v in data.items() })
    for item in data:
        if(item["transaction_type"] == "outcome"):
            soma -= item["value"] 
        if(item["transaction_type"] == "income"):
            soma += item["value"] 
    return f'Seu saldo é de: R$ {soma}'