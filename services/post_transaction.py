import json
from .all_transactions import all_transactions

def post_transaction(filename, title, transaction_type, value):
    data = all_transactions(filename)
    data.append({'title': title, 'transaction_type': transaction_type, 'value': value, 'date': '03/02/2021'}) 
    new_file = open(filename, 'w+')
    json.dump(data, new_file, indent=2)
    new_file.close()
    



