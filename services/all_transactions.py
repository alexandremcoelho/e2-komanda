import json
import os

def all_transactions(filename):
    if not os.path.exists(filename) or os.stat(filename).st_size == 0:
        return []
    current_file = open(filename)
    data = json.load(current_file)
    current_file.close()
    return data
